
export class InventoryItem {
    id: string;
    photoUrl: string;
    quantityInStock: number;
    quantityOnOrder: number;
    estimatedDeliveryDate: Date;
    type: string;
    name: string;
    description: string;
}