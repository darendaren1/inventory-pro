import { InventoryItem } from "./inventory-item";

export class OrderItem
{
    id: string;
    item: InventoryItem;
    quantity: number;
}