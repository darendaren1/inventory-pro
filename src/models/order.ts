import { OrderItem } from "./order-item";

export type OrderStatus = 'Pending' | 'Rejected' | 'Awaiting Delivery' | 'Awaiting Pickup'

export class Order
{
    id: string;
    status: OrderStatus;
    created: Date;
    modified: Date;
    requestingUserId: string;
    projectId: string;
    items: OrderItem[] = [];

    constructor() {
        this.status = 'Pending';
    }
}