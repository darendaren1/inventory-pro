import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { InventoryItem } from '../../models/inventory-item';
import * as firebase from 'firebase';
import "firebase/firestore";

/**
 * Generated class for the CreateNewInventoryItemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-new-inventory-item',
  templateUrl: 'create-new-inventory-item.html',
})
export class CreateNewInventoryItemPage {

  newItem: InventoryItem = new InventoryItem();

  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController) {
    console.log(this.newItem);
    setTimeout(()=> {
      this.newItem.name = 'Newer Test';
      console.log(this.newItem);
    }, 8000)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateNewInventoryItemPage');
  }

  public saveInventoryItem() {
    firebase.firestore().collection('inventory').add(Object.assign({}, this.newItem)).then(()=> {
      this.toastCtrl.create({
        message: 'Your item was successfully saved.',
        duration: 2000
      }).present().then(()=> {
        this.navCtrl.setRoot('AppTabsPage');
      });
    });
  }

}
