import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateNewInventoryItemPage } from './create-new-inventory-item';

@NgModule({
  declarations: [
    CreateNewInventoryItemPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateNewInventoryItemPage),
  ],
})
export class CreateNewInventoryItemPageModule {}
