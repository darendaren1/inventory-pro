import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { CartTracker } from '../../app/cart-tracker';

/**
 * Generated class for the AppTabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-tabs',
  templateUrl: 'app-tabs.html',
})
export class AppTabsPage {

  constructor(public navCtrl: NavController, public toastCtrl: ToastController, public navParams: NavParams, public cartTracker: CartTracker) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppTabsPage');
  }

  ionViewDidEnter() {
    console.log('hello')
    this.cartTracker.cartSubscription.subscribe(items => {
      this.cartCount = 0;
      console.log('In subscription')
      items.forEach(item => {
        console.log(this.cartCount);
        console.log(item.quantity)
        this.cartCount = this.cartCount + item.quantity;
        console.log(this.cartCount);
        console.log(item.quantity)
      })
    })
  }

  tab1Root = "SearchPage";
  tab2Root = "MyRequestsPage";
  tab3Root = "NotificationsPage";
  tab4Root = "CartPage";

  cartCount: number = 0;

  navigateToNewInventoryItem() {
    this.navCtrl.push('CreateNewInventoryItemPage').then(()=> {
      this.toastCtrl.create({
        message: 'Navigating now.',
        duration: 3000,
        position: 'top'
      }).present();
    });
  }
}
