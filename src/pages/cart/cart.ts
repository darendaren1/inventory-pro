import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CartTracker, CartItem } from '../../app/cart-tracker';

/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {

  public cartItems: CartItem[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public cartTracker: CartTracker) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartPage');
  }

  ionViewDidEnter() {
    console.log(this.cartTracker.getCart());
    this.cartItems = this.cartTracker.getCart();
  }

}
