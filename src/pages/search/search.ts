import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { InventoryItem } from '../../models/inventory-item';
import * as firebase from 'firebase';
import "firebase/firestore";
import { CartTracker } from '../../app/cart-tracker';

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  public inventoryItems: InventoryItem[];

  private originalItems: InventoryItem[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public cartTracker: CartTracker) {
   
    this.quantity = 1;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }

  ngOnInit() {
    const loader = this.loadingCtrl.create({
      content: 'Loading inventory...'
    });
    loader.present();
firebase.firestore().collection('inventory').get().then(results => {
  this.inventoryItems = results.docs.map(item => item.data() as InventoryItem);
  this.originalItems = this.inventoryItems;
  loader.dismiss();
}); 


  }

quantity = 5;
filter(event) {

  // This is logging user input to the terminal when the program runs with ionic -c  
  console.log(event.target.value);
  this.inventoryItems = this.originalItems;
  let userInput: string = event.target.value as string;
  userInput = userInput.toLowerCase().trim();
  this.inventoryItems = this.inventoryItems.filter(item => item.name.toLowerCase().includes(userInput) || item.description.toLowerCase().includes(userInput) )
}


addToCart(item: InventoryItem) {
  console.log(item);
  this.cartTracker.addItemToCart(item, 1);
}
}
