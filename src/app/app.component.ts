import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import * as firebase from 'firebase';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = 'AppTabsPage';

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
      // Initialize Firebase
  var config = {
    apiKey: "AIzaSyC8hX2rs2nMfBz5H1edslqG8soh9y10SBo",
    authDomain: "inventorypro-ffa13.firebaseapp.com",
    databaseURL: "https://inventorypro-ffa13.firebaseio.com",
    projectId: "inventorypro-ffa13",
    storageBucket: "inventorypro-ffa13.appspot.com",
    messagingSenderId: "769299187189"
  };
  firebase.initializeApp(config);
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
