import { InventoryItem } from "../models/inventory-item";
import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Subject";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/observable/of';

@Injectable()
export class CartTracker {
    private cart: CartItem[] = [];
    public cartPublisher: Subject<CartItem[]> = new Subject();
    public cartSubscription: Observable<CartItem[]> = this.cartPublisher.asObservable();


    public addItemToCart(item: InventoryItem, quantity: number) {
        this.cart.push({
            inventoryItem: item,
            quantity: quantity
        });
        this.cartPublisher.next(this.cart);
    }

    public getCart() {
        return this.cart;
    }
}

 export class CartItem {
    inventoryItem: InventoryItem;
    quantity: number;
}